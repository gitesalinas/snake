﻿#include "pch.h"
#include <iostream>
#include<windows.h>
#include <conio.h>
#include <ctime>
#include <cstdlib>

#define ROWS 20
#define COLUMNS 40
#define PADDING_ROWS 1
#define PADDING_COLUMNS 18
#define WALL '#'
#define WAY ' '
#define SNAKE_HEAD 'O'
#define SNAKE_BODY 'o'
#define POINTS 10
#define SNAKE_LEFT_COLOR new char[6] { "\033[36m" }
#define SNAKE_RIGHT_COLOR new char[6] { "\033[35m" }
#define RESET_COLOR new char[6] { "\033[0m" }

using namespace std;

enum EDirection { STOP, LEFT, RIGHT, UP, DOWN };

struct Node {
	char graphic;
	EDirection direction;
	int posX;
	int posY;
	Node* nextNode;
};

struct Fruit {
	char graphic;
	int posX;
	int posY;
};


void Play();


void Setup();
void Draw();
void Logic();


void ConsoleSetup();
void CursorSetup();
void GameSetup();


void DrawMap();
void DrawSnake(Node* head, char color[]);
void DrawScore();
void DrawFruit();


bool SnakeCollission(Node* head);
bool BoundaryCollision(Node* head);
void FruitCollission(Node* head, Node*& tale);
void HandleKeyBoardHit();
void HandlePause();
void RecursiveMovement(Node*, Node*);
void UpdatePosition(Node* head);
void ManageFruit();


Node* CreateNode(char, EDirection, int, int);
Fruit* CreateFruit(char, int, int);
void SetCursorPosition(int, int);

bool paused = false;
bool gameOver;
Node* head;
Node* tale;
Fruit* fruit;
int score;

Node* head2;
Node* tale2;


HANDLE handle;
CONSOLE_CURSOR_INFO cursorInfo;

int main()
{
	int option;

	ConsoleSetup();
	CursorSetup();

	do
	{
		system("cls");
		SetCursorPosition(5, 5);
		cout << "######MENU######" << endl;
		SetCursorPosition(5, 6);
		cout << "# 1) Play      #" << endl;
		SetCursorPosition(5, 7);
		cout << "# 2) Exit      #" << endl;
		SetCursorPosition(5, 8);
		cout << "################" << endl;
		SetCursorPosition(5, 9);
		cout << "Choose option:";
		cin >> option;

		if (option == 1) Play();
		
	} while (option != 2);

	system("cls");
	cout << "Thank you for playing!!" << endl;
	system("PAUSE");
}


void Play()
{
	Setup();

	while (!gameOver) {

		Draw();

		Logic();

		Sleep(20);
	}

	SetCursorPosition(0, ROWS + 1);
	system("PAUSE");
}



void Setup() 
{
	GameSetup();
}

void Draw() 
{
	system("cls");

	DrawMap();
	
	DrawSnake(head, SNAKE_LEFT_COLOR);
	DrawSnake(head2, SNAKE_RIGHT_COLOR);
	
	DrawScore();
	DrawFruit();
}

void Logic() 
{
	if (SnakeCollission(head)) return;
	if (SnakeCollission(head2)) return;

	if (BoundaryCollision(head)) return;
	if (BoundaryCollision(head2)) return;

	FruitCollission(head, tale);
	FruitCollission(head2, tale2);

	HandleKeyBoardHit();

	HandlePause();

	RecursiveMovement(head, head->nextNode);
	RecursiveMovement(head2, head2->nextNode);

	UpdatePosition(head);
	UpdatePosition(head2);

	ManageFruit();
}


void ConsoleSetup() 
{
	handle = GetStdHandle(STD_OUTPUT_HANDLE);
}

void CursorSetup()
{
	GetConsoleCursorInfo(handle, &cursorInfo);
	cursorInfo.bVisible = false;
	SetConsoleCursorInfo(handle, &cursorInfo);
}

void GameSetup()
{
	gameOver = false;

	head = tale = CreateNode(SNAKE_HEAD, STOP, 1, ROWS - 2);
	head2 = tale2 = CreateNode(SNAKE_HEAD, STOP, COLUMNS - 2, ROWS - 2);

	score = 0;

	srand(time(0));
}



void DrawMap()
{
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLUMNS; j++) {
			SetCursorPosition(j, i);

			if (i == 0 || (i + 1) == ROWS) cout << WALL;
			else if (j == 0 || (j + 1) == COLUMNS) cout << WALL;
			else cout << WAY;
		}
	}
}

void DrawSnake(Node* head, char color[])
{
	Node* ptr = head;
	while (ptr) {
		SetCursorPosition(ptr->posX, ptr->posY);
		cout << color << ptr->graphic << RESET_COLOR;
		ptr = ptr->nextNode;
	}
}

void DrawScore() 
{
	SetCursorPosition(0, ROWS);
	cout << "Score: " << score;
}

void DrawFruit()
{
	if (fruit == NULL) return;

	SetCursorPosition(fruit->posX, fruit->posY);
	cout << fruit->graphic;
}



bool SnakeCollission(Node* head)
{
	Node* ptr = head->nextNode;
	while (ptr) 
	{
		if (head->posX == ptr->posX && head->posY == ptr->posY)
		{
			gameOver = true;
			return true;
		}

		ptr = ptr->nextNode;
	}

	return false;
}
bool BoundaryCollision(Node* head)
{
	if ((head->posX != 0 && head->posX != COLUMNS - 1) && (head->posY != 0 && head->posY != ROWS - 1)) return false;

	gameOver = true;

	return true;
}
void FruitCollission(Node* head, Node*& tale)
{
	if (fruit == NULL) return;

	if (head->posX == fruit->posX && head->posY == fruit->posY)
	{
		tale->nextNode = CreateNode(SNAKE_BODY, STOP, tale->posX, tale->posY);
		tale = tale->nextNode;

		score += POINTS;

		delete[] fruit;
		fruit = NULL;
	}
}
void HandleKeyBoardHit()
{
	if (_kbhit()) {

		switch (_getch()) {
		case 'a':
		case 'A':
			if(head->direction != RIGHT)
				head->direction = LEFT; break;
		case 'd':
		case 'D':
			if (head->direction != LEFT)
				head->direction = RIGHT; break;
		case 'w':
		case 'W':
			if (head->direction != DOWN)
				head->direction = UP; break;
		case 's':
		case 'S':
			if (head->direction != UP)
				head->direction = DOWN; break;


		case '4':
			if (head2->direction != RIGHT)
				head2->direction = LEFT; break;
		case '6':
			if (head2->direction != LEFT)
				head2->direction = RIGHT; break;
		case '8':
			if (head2->direction != DOWN)
				head2->direction = UP; break;
		case '5':
			if (head2->direction != UP)
				head2->direction = DOWN; break;

		case '.': 
			paused = true;
			break;

		case ' ':
			gameOver = true;
			break;

		default: break;
		}
	}
}
void HandlePause() 
{
	if (paused) {
		char chr;
		SetCursorPosition(COLUMNS - 6, ROWS);
		cout << "PAUSED";
		while (_getch() != '.');
		paused = false;
	}
}

void RecursiveMovement(Node* prevNode, Node* nextNode) {
	if (nextNode == NULL) return;

	RecursiveMovement(nextNode, nextNode->nextNode);

	nextNode->posX = prevNode->posX;
	nextNode->posY = prevNode->posY;
}

void UpdatePosition(Node* head)
{
	if (head->direction == LEFT) head->posX--;
	else if (head->direction == RIGHT) head->posX++;
	else if (head->direction == UP) head->posY--;
	else if (head->direction == DOWN) head->posY++;
}

void ManageFruit()
{
	if(fruit == NULL)
		fruit = CreateFruit('@', 1 + rand() % (COLUMNS - 2), 1 + rand() % (ROWS - 2));
}



Node* CreateNode(char graphic, EDirection direction, int posX, int posY) 
{
	Node* node = new Node();
	node->graphic = graphic;
	node->direction = direction;
	node->posX = posX;
	node->posY = posY;
	node->nextNode = NULL;

	return node;
}

Fruit* CreateFruit(char graphic, int posX, int posY)
{
	Fruit* fruit = new Fruit();
	fruit->graphic = graphic;
	fruit->posX = posX;
	fruit->posY = posY;

	return fruit;
}

void SetCursorPosition(int x, int y)
{
	COORD c;
	c.X = PADDING_COLUMNS  + x;
	c.Y = PADDING_ROWS + y;
	SetConsoleCursorPosition(handle, c);
}

